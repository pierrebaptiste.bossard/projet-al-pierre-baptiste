# Projet AL - Pierre-Baptiste

## My Anecdotes 'Front' - Angular
###### https://gitlab.com/pierrebaptiste.bossard/my-anecdotes-front.git
###### Deploy : Gitlab Pages

## NodeJs_Project 'Back' - NodeJS / Express
###### https://gitlab.com/pierrebaptiste.bossard/nodejs-heroku-gitlab.git
###### Deploy : https://git.heroku.com/nodejs-gitlab-env-staging.git

## Robot API - NodeJS / Express
###### https://gitlab.com/pierrebaptiste.bossard/robot-heroku-gitlab.git
###### Deploy : https://git.heroku.com/robot-gitlab-env-staging.git

## Scraper - NodeJS / Express / Puppeteer
###### https://gitlab.com/pierrebaptiste.bossard/scraper-heroku-gitlab.git

## Spring_Project - Java Spring Boot
###### https://gitlab.com/pierrebaptiste.bossard/spring-heroku-gitlab.git
###### Deploy : https://git.heroku.com/spring-gitlab-env-staging.git

Lien de l'application : http://anecnews.cf/ (suspendu)
